﻿using HomeworkService.Consumers.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using HomeworkService.Core.Services;
using HomeworkService.Data;
using HomeworkService.Data.Repositories;
using HomeworkService.Core.DTO;
using HomeworkService.Core.Entities;
using HomeworkService.Core.Infrastructure;
using HomeworkService.Core.Interfaces;
using HomeworkService.Data.Infrastructure;
using HomeworkService.Instance.Infrastructure.MessageBroker;
using HomeworkService.Instance.Infrastructure.Swagger;
using Microsoft.EntityFrameworkCore;

namespace HomeworkService.Instance
{
    /// <summary>
    /// Инициализация приложения.
    /// </summary>
    public class Startup
    {
        private readonly IConfiguration configuration;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="Startup"/>.
        /// </summary>
        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            this.configuration = configuration;
            this.ConfigureDataStorage();
        }

        /// <summary>
        /// Конфигурация приложения.
        /// </summary>
        /// <param name="application">Приложение.</param>
        public void Configure(IApplicationBuilder application)
        {
            application.UseCors("AllowAll");
            application.UseMvc();
            application.UseSwaggerDocumentation(this.configuration);
        }

        /// <summary>
        /// Конфигурация сервисов.
        /// </summary>
        /// <param name="services">Сервисы.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(this.configuration)
                .CreateLogger();

            Log.Information("Начинается регистрация политик CORS.");

            services.AddCors(o => o.AddPolicy("AllowAll", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));

            Log.Information("Регистрация политик CORS успешно завершена.");

            Log.Information("Начинается регистрация Swagger генератора.");

            services.AddSwaggerDocumentation(this.configuration);

            Log.Information("Регистрация Swagger генератора завершена.");

            Log.Information("Начинается регистрация сервисов.");

            services.AddScoped<DbContext, HomeworkContext>();
            services.AddDbContext<HomeworkContext>(option =>
                option.UseNpgsql(this.configuration.GetConnectionString("Homeworks")));
            services.AddScoped<IServiceAsync<Homework>, HomeworkServiceAsync>();
            services.AddScoped<IRepositoryAsync<HomeworkDto>, EFRepository<HomeworkDto>>();

            //TODO: исправить эту сраную ситуацию

            //services.AddCore();

            //services.AddData();

            services.AddConsumers();

            Log.Information("Регистрация сервисов успешно завершена.");

            Log.Information("Начинается регистрация шины.");

            services.BusRegistration(this.configuration);

            Log.Information("Регистрация шины успешно завершена.");

            Log.Information("Начинается регистрация фильтра авторизации.");

            Log.Information("Начинается регистрация сервисов MVC.");

            services.AddMvc();

            Log.Information("Регистрация сервисов MVC успешно завершена.");
        }

        /// <summary>
        /// Конфигурирует базу данных.
        /// </summary>
        public void ConfigureDataStorage()
        {
            Log.Information("Начинается создание/обновление базы данных.");

            var contextOptions = new DbContextOptionsBuilder<HomeworkContext>();
            contextOptions.UseNpgsql(this.configuration.GetConnectionString("Homeworks"));

            using (var context = new HomeworkContext(contextOptions.Options))
            {
                context.Database.EnsureCreated();
            }

            Log.Information("Создание/обновление базы данных завершено.");
        }
    }
}