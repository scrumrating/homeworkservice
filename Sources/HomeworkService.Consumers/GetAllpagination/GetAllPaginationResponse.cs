﻿using System.Collections.Generic;
using HomeworkService.Core.Entities;

namespace HomeworkService.Consumers.GetAllpagination
{
    /// <summary>
    /// Ответ на команду получения списка.
    /// </summary>
    public class GetAllPaginationResponse
    {
        /// <summary>
        /// Получает или задает список.
        /// </summary>
        public IEnumerable<Homework> Homeworks { get; set; }

        /// <summary>
        /// Получает или задает результат обработки команды.
        /// </summary>
        public string Result { get; set; }
    }
}