﻿using System.Threading.Tasks;
using HomeworkService.Core.Constants;
using HomeworkService.Core.Entities;
using HomeworkService.Core.Interfaces;
using MassTransit;
using Microsoft.Extensions.Logging;

namespace HomeworkService.Consumers.GetAll
{
    /// <summary>
    /// Обработчик сообщения получения списка.
    /// </summary>
    public class GetAllConsumer : IConsumer<GetAllCommand>
    {
        private readonly IServiceAsync<Homework> service;
        private readonly ILogger<GetAllConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="GetAllConsumer"/>.
        /// </summary>
        /// <param name="service">Сервисный объект.</param>
        /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public GetAllConsumer(IServiceAsync<Homework> service, ILogger<GetAllConsumer> logger)
        {
            this.service = service;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<GetAllCommand> context)
        {
            this.logger.LogInformation("Выполняется обработка сообщения получения всех записей.");

            var homeworks = await this.service.GetAsync();

            if (homeworks != null)
            {
                await context.RespondAsync(new GetAllResponse { Homeworks = homeworks, Result = ResponseResult.Success });
                return;
            }

            await context.RespondAsync(new GetAllResponse { Result = ResponseResult.NotSuccess });
        }
    }
}