﻿using System.Collections;
using System.Collections.Generic;
using HomeworkService.Core.Entities;

namespace HomeworkService.Consumers.GetAll
{
    /// <summary>
    /// Ответ на команду получения списка домашних заданий.
    /// </summary>
    public class GetAllResponse
    {
        /// <summary>
        /// Получает или задает список домашних заданий.
        /// </summary>
        public IEnumerable<Homework> Homeworks { get; set; }

        /// <summary>
        /// Получает или задает результат обработки команды.
        /// </summary>
        public string Result { get; set; }
    }
}