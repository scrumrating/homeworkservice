﻿using HomeworkService.Core.Entities;

namespace HomeworkService.Consumers.GetById
{
    /// <summary>
    /// Ответ на команду получения данных по id.
    /// </summary>
    public class GetByIdResponse
    {
        /// <summary>
        /// Получает или задаёт данные о домашнем задании.
        /// </summary>
        public Homework Homework { get; set; }
        
        /// <summary>
        /// Получает или задаёт результат обработки команды.
        /// </summary>
        public string Result { get; set; }
    }
}