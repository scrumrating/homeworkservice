﻿using System.Threading.Tasks;
using HomeworkService.Core.Constants;
using HomeworkService.Core.Entities;
using HomeworkService.Core.Interfaces;
using MassTransit;
using Microsoft.Extensions.Logging;

namespace HomeworkService.Consumers.Create
{
    /// <summary>
    /// Обработчик сообщения создания.
    /// </summary>
    public class CreateConsumer : IConsumer<CreateCommand>
    {
        private readonly IServiceAsync<Homework> service;
        private readonly ILogger<CreateConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="CreateConsumer"/>.
        /// </summary>
        /// <param name="service">Сервисный объект.</param>
        /// /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public CreateConsumer(IServiceAsync<Homework> service, ILogger<CreateConsumer> logger)
        {
            this.service = service;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<CreateCommand> context)
        {
            this.logger.LogInformation($"Выполняется обработка сообщения создания '{context.Message.Homework}'");

            var result = await this.service.CreateAsync(context.Message.Homework);

            if (result)
            {
                await context.RespondAsync(new CreateResponse() {Result = ResponseResult.Success});
                return;
            }

            await context.RespondAsync(new CreateResponse {Result = ResponseResult.NotSuccess});
        }
    }
}