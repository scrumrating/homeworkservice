﻿using HomeworkService.Core.Entities;

namespace HomeworkService.Consumers.Create
{
    /// <summary>
    /// Команда для создания домашнего задания.
    /// </summary>
    public class CreateCommand
    {
        /// <summary>
        /// Получает или задает данные о домашнем задании.
        /// </summary>
        public Homework Homework { get; set; }
    }
}