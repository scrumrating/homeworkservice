﻿using System.Diagnostics.CodeAnalysis;
using MassTransit;
using Microsoft.Extensions.DependencyInjection;

namespace HomeworkService.Consumers.Infrastructure
{
    /// <summary>
    /// Регистрация сервисов
    /// </summary>
    [ExcludeFromCodeCoverage]
    public static class ServiceRegistration
    {
        /// <summary>
        /// Добавляет сервисы сборки в контейнер.
        /// </summary>
        /// <param name="services">Сервисы контейнера.</param>
        /// <returns>Изменённый набор сервисов контейнера.</returns>
        public static IServiceCollection AddConsumers(this IServiceCollection services)
        {
            services.Scan(scan => scan
                .FromExecutingAssembly()
                .AddClasses(classes => classes.AssignableTo(typeof(IConsumer<>)))
                .AsImplementedInterfaces()
                .WithScopedLifetime());
            return services;
        }
    }
}