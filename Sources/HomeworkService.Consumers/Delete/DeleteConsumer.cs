﻿using System.Threading.Tasks;
using HomeworkService.Core.Constants;
using HomeworkService.Core.Entities;
using HomeworkService.Core.Interfaces;
using MassTransit;
using Microsoft.Extensions.Logging;

namespace HomeworkService.Consumers.Delete
{
    /// <summary>
    /// Обработчик сообщения получения удаления данных по Id.
    /// </summary>
    public class DeleteConsumer : IConsumer<DeleteCommand>
    {
        private readonly IServiceAsync<Homework> service;
        private readonly ILogger<DeleteConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="DeleteConsumer"/>.
        /// </summary>
        /// <param name="service">Сервисный объект.</param>
        /// /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public DeleteConsumer(IServiceAsync<Homework> service, ILogger<DeleteConsumer> logger)
        {
            this.service = service;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<DeleteCommand> context)
        {
            this.logger.LogInformation($"Выполняется обработка сообщения удаления данных по Id '{context.Message.Id}'");

            await this.service.RemoveAsync(context.Message.Id);
            await context.RespondAsync(new DeleteResponse() {Result = ResponseResult.Success});
        }
    }
}