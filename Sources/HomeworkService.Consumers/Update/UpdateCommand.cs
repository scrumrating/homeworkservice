﻿using HomeworkService.Core.Entities;

namespace HomeworkService.Consumers.Update
{
    /// <summary>
    /// Команда для обновления данных о домашнем задании.
    /// </summary>
    public class UpdateCommand
    {
        /// <summary>
        /// Получает или задает данные о домашнем задании.
        /// </summary>
        public Homework Homework { get; set; }
    }
}