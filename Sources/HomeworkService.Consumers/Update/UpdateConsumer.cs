﻿using System.Threading.Tasks;
using HomeworkService.Core.Constants;
using HomeworkService.Core.Entities;
using HomeworkService.Core.Interfaces;
using MassTransit;
using Microsoft.Extensions.Logging;

namespace HomeworkService.Consumers.Update
{
    /// <summary>
    /// Обработчик сообщения обновления данных.
    /// </summary>
    public class UpdateConsumer : IConsumer<UpdateCommand>
    {
        private readonly IServiceAsync<Homework> service;
        private readonly ILogger<UpdateConsumer> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="UpdateConsumer"/>.
        /// </summary>
        /// <param name="service">Сервисный объект.</param>
        /// /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public UpdateConsumer(IServiceAsync<Homework> service, ILogger<UpdateConsumer> logger)
        {
            this.service = service;
            this.logger = logger;
        }

        /// <summary>
        /// Обработать сообщение.
        /// </summary>
        /// <param name="context">Контекст обработки сообщения.</param>
        /// <returns>Асинхронная операция <see cref="Task"/>.</returns>
        public async Task Consume(ConsumeContext<UpdateCommand> context)
        {
            this.logger.LogInformation($"Выполняется обработка сообщения обновления данных '{context.Message.Homework}'.");

            await this.service.UpdateAsync(context.Message.Homework);
            await context.RespondAsync(new UpdateResponse { Result = ResponseResult.Success });
        }
    }
}