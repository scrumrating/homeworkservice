﻿using System;

namespace HomeworkService.Core.Entities
{
    /// <summary>
    /// Бизнес модель сущности домашнее задание.
    /// </summary>
    public class Homework
    {
        /// <summary>
        /// Получает или задаёт идентификатор записи.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Получает или задаёт название домашнего задания.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Получает или задаёт описание домашнего задания.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Получает или задаёт множетель домашнего задания.
        /// </summary>
        public int Factor { get; set; }
        
        /// <summary>
        /// Получает или задаёт id урока.
        /// </summary>
        public int LessonId { get; set; }

        /// <summary>
        /// Получает или задаёт дату сдачи домашнего задания.
        /// </summary>
        public DateTime DeadLine { get; set; }

        /// <summary>
        /// Инициализирует экземпляр класса <see cref="Homework"/>
        /// </summary>
        public Homework()
        {
        }

        /// <summary>
        /// Инициализирует экземпляр класса <see cref="Homework"/>
        /// </summary>
        /// <param name="id">Идентификатор записи.</param>
        /// <param name="name">Название домашнего задания.</param>
        /// <param name="description">Описание домашнего задания.</param>
        /// <param name="factor">Множетель домашнего задания.</param>
        /// <param name="deadLine">Срок сдачи домашнего задания.</param>
        public Homework(int id, string name, string description, int factor, int lessonId, DateTime deadLine)
        {
            Id = id;
            Name = name;
            Description = description;
            Factor = factor;
            LessonId = lessonId;
            DeadLine = deadLine;
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            var isEqual = false;
            switch (obj)
            {
                case null:
                    throw new NullReferenceException();

                case Homework homework:
                {
                    var compareToObj = homework;

                    if (compareToObj.Id.CompareTo(this.Id) == 0)
                    {
                        isEqual = true;
                    }

                    break;
                }

                case string name:
                {
                    if (string.Compare(name, this.Name, StringComparison.Ordinal) == 0)
                    {
                        isEqual = true;
                    }

                    break;
                }
            }

            return isEqual;
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return
                $"Id = {this.Id}, Name = {this.Name}, Description = {this.Description}, LessonId = {this.LessonId} Factor = {this.Factor}, DeadLine = {this.DeadLine.Date}";
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }
    }
}