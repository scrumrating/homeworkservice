﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using HomeworkService.Core.DTO;
using HomeworkService.Core.Entities;
using HomeworkService.Core.Interfaces;
using Microsoft.Extensions.Logging;

namespace HomeworkService.Core.Services
{
    public class HomeworkServiceAsync : IServiceAsync<Homework>
    {
        private readonly IRepositoryAsync<HomeworkDto> repository;
        private readonly ILogger<HomeworkServiceAsync> logger;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="HomeworkServiceAsync"/>.
        /// </summary>
        /// <param name="repositoryAsync">Хранилище данных о студентах.</param>
        /// <param name="logger">Абстракция над сервисом журналирования.</param>
        public HomeworkServiceAsync(IRepositoryAsync<HomeworkDto> repository, ILogger<HomeworkServiceAsync> logger)
        {
            this.repository = repository;
            this.logger = logger;
        }

        /// <inheritdoc />
        public async Task<bool> CreateAsync(Homework item)
        {
            this.logger.LogInformation($"Выполняется создание {item}.");

            await this.repository.CreateAsync(new HomeworkDto()
            {
                Name = item.Name,
                Description = item.Description,
                LessonId = item.LessonId,
                Factor = item.Factor,
                DeadLine = item.DeadLine,
            });
            return true;
        }

        /// <inheritdoc />
        public async Task<Homework> FindByIdAsync(int id)
        {
            this.logger.LogInformation($"Выполняется поиск с id {id}.");
            var homework = await this.repository.FindByIdAsync(id);
            Homework resultHomework = null;
            if (homework != null)
            {
                resultHomework = new Homework()
                {
                    Id = homework.Id,
                    Name = homework.Name,
                    Description = homework.Description,
                    LessonId = homework.LessonId,
                    Factor = homework.Factor,
                    DeadLine = homework.DeadLine,
                };
            }

            return resultHomework;
        }

        /// <inheritdoc />
        public async Task<IEnumerable<Homework>> GetAsync()
        {
            this.logger.LogInformation($"Выполняется поиск всех записей.");
            var homeworks = await this.repository.GetAsync();
            return homeworks.Select(homework => new Homework()
            {
                Id = homework.Id,
                Name = homework.Name,
                Description = homework.Description,
                LessonId = homework.LessonId,
                Factor = homework.Factor,
                DeadLine = homework.DeadLine,
            });
        }

        /// <inheritdoc />
        public async Task<IEnumerable<Homework>> GetAsync(int pageSize, int pageNumber)
        {
            this.logger.LogInformation($"Выполняется поиск всех записей с количеством {pageSize}, номером страницы {pageNumber}.");
            var homeworks = await this.repository.GetAsync(pageSize, pageNumber);
            return homeworks.Select(homework => new Homework()
            {
                Id = homework.Id,
                Name = homework.Name,
                Description = homework.Description,
                LessonId = homework.LessonId,
                Factor = homework.Factor,
                DeadLine = homework.DeadLine,
            });
        }

        /// <inheritdoc />
        public async Task<IEnumerable<Homework>> GetAsync(Expression<Func<Homework, bool>> predicate)
        {
            //TODO: Доделать работу с предикатом. Тоесть конвертировать в Dto.
            this.logger.LogInformation($"Выполняется поиск всех записей с условием {predicate.Body}.");
            var homeworks = await this.repository.GetAsync();
            return homeworks.Select(homework => new Homework()
            {
                Id = homework.Id,
                Name = homework.Name,
                Description = homework.Description,
                LessonId = homework.LessonId,
                Factor = homework.Factor,
                DeadLine = homework.DeadLine,
            });
        }

        /// <inheritdoc />
        public async Task RemoveAsync(int id)
        {
            this.logger.LogInformation($"Выполняется удаление с Id {id}.");
            await this.repository.RemoveAsync(id);
        }

        /// <inheritdoc />
        public async Task UpdateAsync(Homework item)
        {
            this.logger.LogInformation($"Выполняется обновление данных {item}.");
            await this.repository.UpdateAsync(new HomeworkDto()
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description,
                LessonId = item.LessonId,
                Factor = item.Factor,
                DeadLine = item.DeadLine,
            });
        }
    }
}