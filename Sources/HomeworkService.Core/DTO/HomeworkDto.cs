﻿using System;

namespace HomeworkService.Core.DTO
{
    /// <summary>
    /// Класс модель домашнего задания.
    /// </summary>
    public class HomeworkDto : IdentityDto
    {
        /// <summary>
        /// Получает или задаёт название домашнего задания.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Получает или задаёт описание домашнего задания.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Получает или задаёт множетель домашнего задания.
        /// </summary>
        public int Factor { get; set; }

        /// <summary>
        /// Получает или задаёт id урока.
        /// </summary>
        public int LessonId { get; set; }

        /// <summary>
        /// Получает или задаёт дату сдачи домашнего задания.
        /// </summary>
        public DateTime DeadLine { get; set; }
    }
}