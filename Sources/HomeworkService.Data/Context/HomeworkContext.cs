﻿using HomeworkService.Core.DTO;
using Microsoft.EntityFrameworkCore;

namespace HomeworkService.Data
{
    /// <summary>
    /// Контекст базы данных.
    /// </summary>
    public class HomeworkContext : DbContext
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="HomeworkContext"/>.
        /// </summary>
        public HomeworkContext(DbContextOptions options) : base(options)
        {
        }

        /// <summary>
        /// Получает или задает содержимое базы данных Placeholder.
        /// </summary>
        public DbSet<HomeworkDto> Homeworks { get; set; }
    }
}