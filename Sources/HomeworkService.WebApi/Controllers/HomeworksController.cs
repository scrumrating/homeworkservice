﻿using MassTransit;
using Microsoft.AspNetCore.Mvc;
using HomeworkService.Consumers;
using System;
using System.Net;
using System.Threading.Tasks;
using HomeworkService.Consumers.Create;
using HomeworkService.Consumers.Delete;
using HomeworkService.Consumers.GetAll;
using HomeworkService.Consumers.GetAllpagination;
using HomeworkService.Consumers.GetById;
using HomeworkService.Consumers.Update;
using HomeworkService.Core.Constants;
using HomeworkService.Core.Entities;

namespace HomeworkService.WebApi.Controllers
{
    /// <summary>
    /// Контроллер Homeworks.
    /// </summary>
    [Route("/api/homework")]
    public class HomeworksController : Controller
    {
        private readonly IRequestClient<CreateCommand> createClient;
        private readonly IRequestClient<DeleteCommand> deleteClient;
        private readonly IRequestClient<GetAllCommand> getAllClient;
        private readonly IRequestClient<GetAllPaginationCommand> getAllPaginationClient;
        private readonly IRequestClient<GetByIdCommand> getByIdClient;
        private readonly IRequestClient<UpdateCommand> updateClient;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="HomeworksController"/>.
        /// </summary>
        /// <param name="createClient">Клиент создания.</param>
        /// <param name="deleteClient">Клиент удаления.</param>
        /// <param name="getAllClient">Клиент получения всех записей.</param>
        /// <param name="getAllPaginationClient">Клиент получения записей с пагинацией.</param>
        /// <param name="getByIdClient">Клиент получения записи по id.</param>
        /// <param name="updateClient">Клиент обновления записи.</param>
        public HomeworksController(
            IRequestClient<CreateCommand> createClient,
            IRequestClient<DeleteCommand> deleteClient,
            IRequestClient<GetAllCommand> getAllClient,
            IRequestClient<GetAllPaginationCommand> getAllPaginationClient,
            IRequestClient<GetByIdCommand> getByIdClient,
            IRequestClient<UpdateCommand> updateClient)
        {
            this.createClient = createClient;
            this.deleteClient = deleteClient;
            this.getAllClient = getAllClient;
            this.getAllPaginationClient = getAllPaginationClient;
            this.getByIdClient = getByIdClient;
            this.updateClient = updateClient;
        }

        /// <summary>
        /// Метод для получения доступным методов.
        /// </summary>
        /// <returns>Список доступных методов.</returns>
        [HttpOptions]
        public IActionResult Options()
        {
            this.Response.Headers.Add("Allow", "GET, OPTIONS");
            return this.Ok();
        }

        /// <summary>
        /// Получить список домашних заданий.
        /// </summary>
        /// <returns>Список Homeworks.</returns>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var response = await this.getAllClient.GetResponse<GetAllResponse>(new GetAllCommand());

                switch (response.Message.Result)
                {
                    case ResponseResult.Success:
                        return this.Ok(response.Message.Homeworks);
                    case ResponseResult.NotSuccess:
                        return new StatusCodeResult((int)HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Получить список Homeworks. (Пагинация).
        /// </summary>
        /// <param name="pageSize">Количество строк для получения.</param>
        /// <param name="pageNumber">Номер страницы.</param>
        /// <returns>Список Homeworks.</returns>
        [HttpGet]
        [Route("All")]
        public async Task<IActionResult> Get([FromQuery] int pageSize, [FromQuery] int pageNumber)
        {
            try
            {
                var command = new GetAllPaginationCommand()
                {
                    PageSize = pageSize,
                    PageNumber = pageNumber,
                };

                var response = await this.getAllPaginationClient.GetResponse<GetAllPaginationResponse>(command);

                switch (response.Message.Result)
                {
                    case ResponseResult.Success:
                        return this.Ok(response.Message.Homeworks);
                    case ResponseResult.NotSuccess:
                        return new StatusCodeResult((int)HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Получить домашнее задание по её идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор.</param>
        /// <returns>Объект найденой записи.</returns>
        [HttpGet]
        [Route("GetById")]
        public async Task<IActionResult> Get([FromQuery(Name = "id")] int id)
        {
            try
            {
                var response =
                    await this.getByIdClient.GetResponse<GetByIdResponse>(new GetByIdCommand() { Id = id });

                switch (response.Message.Result)
                {
                    case ResponseResult.Success:
                        return this.Ok(response.Message.Homework);
                    case ResponseResult.NotSuccess:
                        return new StatusCodeResult((int)HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Создание домашнего задания.
        /// </summary>
        /// <param name="homework">Объект домашнего задания.</param>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Homework homework)
        {
            try
            {
                var response =
                    await this.createClient.GetResponse<CreateResponse>(new CreateCommand() { Homework = homework });

                switch (response.Message.Result)
                {
                    case ResponseResult.Success:
                        return this.Ok();
                    case ResponseResult.NotSuccess:
                        return new StatusCodeResult((int)HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Обновление данных домашнего задания.
        /// </summary>
        /// <param name="homework">Объект домашнего задания.</param>
        [HttpPut]
        public async Task<IActionResult> Put([FromBody] Homework homework)
        {
            try
            {
                var response =
                    await this.updateClient.GetResponse<UpdateResponse>(new UpdateCommand() { Homework = homework });

                switch (response.Message.Result)
                {
                    case ResponseResult.Success:
                        return this.Ok();
                    case ResponseResult.NotSuccess:
                        return new StatusCodeResult((int)HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Удаление домашнего задания.
        /// </summary>
        /// <param name="id">Идентификатор для удаления.</param>
        [HttpDelete]
        public async Task<IActionResult> Delete([FromQuery(Name = "id")] int id)
        {
            try
            {
                var response = await this.deleteClient.GetResponse<DeleteResponse>(
                    new DeleteCommand()
                    {
                        Id = id,
                    });

                switch (response.Message.Result)
                {
                    case ResponseResult.Success:
                        return this.Ok();
                    case ResponseResult.NotSuccess:
                        return new StatusCodeResult((int)HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }
    }
}